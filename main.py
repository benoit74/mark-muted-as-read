#!/usr/bin/python3

"""
Mark all muted threads as read.
"""
from __future__ import print_function
import logging
from time import gmtime, strftime
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
import os

logging.basicConfig(level='INFO')

logging.info('---- Mark Muted Mails as read ----')
logging.info(strftime("%Y-%m-%d %H:%M:%S", gmtime()))

dir_name = os.path.dirname(os.path.realpath(__file__))

# Setup the Gmail API
SCOPES = 'https://www.googleapis.com/auth/gmail.modify'
store = file.Storage(os.path.join(dir_name,'credentials.json'))
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets(os.path.join(dir_name,'client_secret.json'), SCOPES)
    creds = tools.run_flow(flow, store)
service = build('gmail', 'v1', http=creds.authorize(Http()), cache_discovery=False)

# Call the Gmail API
results = service.users().threads().list(userId='me', q='label:mute is:unread').execute()
threads = results.get('threads', [])
if not threads:
    print('No muted unread threads found')
else:
    print('Procesing threads:')
    msg_labels = {'removeLabelIds': ['UNREAD'], 'addLabelIds': []}
    for thread in threads:
        print('ID ' + thread['id'])
        service.users().threads().modify(userId='me', id=thread['id'],
                                              body=msg_labels).execute(num_retries=3)


