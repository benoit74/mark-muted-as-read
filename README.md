Ce projet permet de marquer tous les emails GMail d'un compte 'as read' lorsqu'ils sont 'muted'.

# Prérequis :
- Python3 + pip3
- Google API Python Client : pip3 install --upgrade google-api-python-client

# Création d'un projet et d'un compte de service pour requêter les API Google
Tout d'abord, il est nécessaire de créer un projet d'API (structure Google qui recence les appels d'API d'une application, et qui sert à savoir qui facturer si besoin, qui contacter en cas de problème, ...) et un compte de service pour utiliser ce projet (moyen technique d'authentifier le script Python de notre application). Ci-dessous le détail de la procédure. En résumé, il faut un projet avec les APIs GMail et un compte de service de type 'Autre'.

- Se connecter sur https://console.developers.google.com/cloud-resource-manager?folder=&organizationId=485686031058
- Créer un nouveau projet d'API (bouton 'Créer un projet')
- Choisir un nom (e.g. mark-muted-as-read)
- Créer le projet et attendre sa création complète (environ 5-10 secs, cf. zone de notification en haut à droite)
- Une fois le projet créer, l'ouvrir en cliquant dessus
- Dans le menu (les trois barres horizontales en haut à gauche), sélectionner 'API et services' et 'Tableau de bord'
- Cliquer 'Activer des Services'
- Rechercher 'GMail API' dans la liste
- Cliquer sur le bouton 'Activer' pour activer cette API sur votre projet
- Attendre que l'activation soit effective puis revenir sur 'API et services' et 'Identifiants' dans le menu en haut à gauche
- Aller dans l'onglet 'Ecran d'autorisation OAuth'
- Type d'application : 'Interne'
- Nom d'application : un nom qui veut dire quelque chose pour vous (par exemple 'Mark Muted as Read')
- Cliquer sur "Enregistrer" en bas du formulaire (tout le reste peut rester par défaut)
- Aller dans l'onglet 'Identifiants'
- Cliquer sur 'Créer des Identifiants' et choisir 'ID Client OAuth'
- Choisir comme type d'application 'Autre'
- Choisir un nom pour cet Identifiant (e.g. 'Main')
- Ignorer la popup avec vos identifiants
- Sur la ligne de votre identifiant, cliquer sur le bouton "Télécharger"
- Enregistrer le fichier sous le nom 'client_secret.json' dans le même dossier que le script 'main.py' de cette application

# Autorisation du script sur votre compte Google
Maintenant que l'on a un compte de service pour authentifier notre script, il s'agit d'autoriser notre script à accéder à notre boîte email. Ci-dessous le détail de la procédure.

- lancer `main.py --noauth_local_webserver`
- ouvrir votre navigateur avec l'URL fournie par le script
- connecter vous avec le compte 'ki-va-bien'
- autoriser l'application à lire et modifier vos emails
- récupérer le code dans votre navigateur et coller le dans la fenêtre du terminal qui vous le demande
- normalement, un fichier 'credentials.json' est généré et le script s'exécute sur votre boîte mail
- sinon ... bon débuggage !
- pour les fois suivantes, il suffit d'appeler `main.py` sans paramètres, et c'est tout

# Remarques
- un même projet d'API et le compte de service peuvent être réutilisés pour plusieurs comptes emails. Par contre les appels à l'API seront décomptés sur ce projet, les usages abusifs également, etc ... Si vous souhaitez utiliser le même compte de service sur plusieurs comptes mails, il suffit de duppliquer le dossier avec 'main.py' et 'client_secret.json' et de relancer la partie 'Autorisation du script sur votre compte Google' en sélectionnant un autre compte
- ce script est à appeler toutes les x minutes pour marquer les nouveaux emails 'as read'. Personnellement je le lance toutes les 5 minutes avec un timer systemd (il faut juste lancer `main.py`, c'est tout). Pour les plus old-school, une ligne dans un Cron peut faire l'affaire.

 


